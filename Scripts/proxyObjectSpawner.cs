﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class proxyObjectSpawner : MonoBehaviour
{

    [SerializeField] private GameObject prefab;


    // Start is called before the first frame update
    void Awake()
    {
        if(Object.FindObjectsOfType<fullBodyAssign>().Length < 2)
        {
            Instantiate(prefab, transform.position, transform.rotation);
        }
        Destroy(gameObject);
    }

}
