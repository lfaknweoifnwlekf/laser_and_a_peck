﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// NOTE: this only works in the one orientation (Z-)
public class Slider : MonoBehaviour
{

    [SerializeField] private Player.sliderSettings sliderControls;
    [SerializeField] private Transform handle;
     private Player player;


    private float defaultDistance = 1;


    // Start is called before the first frame update
    void Start()
    {
        player = Object.FindObjectOfType<Player>();
        defaultDistance = Vector3.Distance(transform.position, handle.position);
        if (sliderControls == Player.sliderSettings.music) moveHandle(player.volumeMusic);
        else if (sliderControls == Player.sliderSettings.volume) moveHandle(player.volumeMaster);
    }

    // Update is called once per frame
    void Update()
    {
        if (handle.hasChanged)
        {            
            float amount = Vector3.Distance(transform.position, handle.position) / defaultDistance;
            if (handle.position.z > transform.position.z) amount = 0; // prevent negatives
            if (amount > 1) amount = 1;
            player.sliderChange(amount, sliderControls);
            transform.localScale = new Vector3(amount,amount,amount);
            handle.hasChanged = false;         
        }
    }

    public void moveHandle(float amount)
    {
        if (amount > 1) amount = 1;
        else if (amount < 0) amount = 0;
        handle.SetPositionAndRotation(new Vector3(handle.position.x, handle.position.y, transform.position.z- (amount * defaultDistance)),handle.rotation);
    }
}
