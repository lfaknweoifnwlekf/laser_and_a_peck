﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ZoneGroup : MonoBehaviour
{
    public bool activated = false;
    private Zone[] zones;

    private bool initialized = false;
    
    [Tooltip("Action that happens when all zones are filled")] [SerializeField] UnityEvent triggerAction = new UnityEvent();
    [Tooltip("Optional action that happens after all zones are filled but then are deactivated")] [SerializeField] UnityEvent triggerExitAction = new UnityEvent();

    private AudioSource aud;
    [SerializeField] private AudioClip enableSound;
    [SerializeField] private AudioClip disableSound;

    private Player player;

    [SerializeField] bool debug = false;

    // Start is called before the first frame update
    void Start()
    {
        zones = GetComponentsInChildren<Zone>();
        initialized = true;
        player = Object.FindObjectOfType<Player>();

        // audio
        aud = GetComponent<AudioSource>();
        if (aud == null && (enableSound != null || disableSound != null))
        {
            aud = gameObject.AddComponent<AudioSource>();
            aud.spatialBlend = .95F;
            aud.minDistance = .1F;
            aud.maxDistance = 30;
        }
    }

    // Update is called once per frame
    private void OnEnable()
    {
        if(initialized) resetZones();
    }

    private void Update()
    {
        if (debug) trigger();
    }

    // when a zone changes status
    public void trigger()
    {
        if (testZones())
        {
            if (enableSound != null) AudioSource.PlayClipAtPoint(enableSound,transform.position);
            triggerAction.Invoke();
            activated = true;
        }
        else
        {
            if (activated && triggerExitAction != null)
            {
                triggerExitAction.Invoke();
                if (disableSound != null) AudioSource.PlayClipAtPoint(disableSound, transform.position) ;
            }
            activated = false;
        }
    }

    private bool testZones()
    {
        foreach (Zone z in zones)
        {
            if (!z.activated) return false;
        }
        return true;
    }

    public void resetZones()
    {
        foreach (Zone z in zones)
        {
            if(!z.activatedAlways) z.activated = false;
        }
    }
}
