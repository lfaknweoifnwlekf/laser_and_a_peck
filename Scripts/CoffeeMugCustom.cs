﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CoffeeMugCustom : MonoBehaviour {

    public bool isFull = false;

    private Player player;

    public float fillTime = 2;

    private float fillAmount = 0; // 0 to 1
    private bool isFilling = false;
    [SerializeField] private AudioClip clinkSound;
    [SerializeField] private AudioClip sipSound;
    [SerializeField] private AudioClip fillSound;
    [SerializeField] private AudioClip fullSound;
    private AudioSource sounds;
    [SerializeField] private GameObject steam;
    private Collider[] colliders;
    private Vector3 spawnPos;
    private Rigidbody rigid;

    private SkinnedMeshRenderer shapekeys;

    [SerializeField] UnityEvent filledAction = new UnityEvent();
    [SerializeField] UnityEvent drinkAction = new UnityEvent();

    // head  collision
    private void OnTriggerEnter(Collider other) {
        //Debug.Log(isFull+ "Coffee enter: " +other.GetComponentInParent<Rigidbody>().gameObject.name);
        if(isFull && GameObject.ReferenceEquals(other.GetComponentInParent<Rigidbody>().gameObject,player.gameObject)) {
            sounds.Stop();
            sounds.clip = sipSound;
            sounds.Play();
            player.victory();
            StartCoroutine(emptySlowly());
            drinkAction.Invoke();
        }
    }

    // general noise collissoin
    private void OnCollisionEnter(Collision collision)
    {
        if (!isFilling && collision.transform.tag != "Player")
        {
            sounds.Stop();
            sounds.clip = clinkSound;
            sounds.Play();
        }
    }


    public void activated(bool on) {
        isFilling = on;
        if (on)
        {
            StopCoroutine(emptySlowly());
            if(sounds.clip != fillSound)
            {
                sounds.Stop();
                sounds.clip = fillSound;
            }
            sounds.Play();
        }
        else sounds.Pause();
    }

    // Use this for initialization
    void Start () {
        shapekeys = GetComponent<SkinnedMeshRenderer>();
        sounds = GetComponent<AudioSource>();
        player = Object.FindObjectOfType<Player>();
        colliders = GetComponentsInChildren<Collider>();
        rigid = GetComponent<Rigidbody>();
        spawnPos = transform.position + new Vector3(0, .15F, 0);
    }

    IEnumerator emptySlowly()
    {
        isFull = false;
        isFilling = false;
        steam.SetActive(false);
        for (int i = 0; i < 10; i++)
        {
            yield return new WaitForSeconds(.1F);
            fillAmount -= .1F;
            shapekeys.SetBlendShapeWeight(0, fillAmount * 100);
        }
    }


    // Update is called once per frame
    void Update () {

        if (isFilling && !isFull)
        {
            StopCoroutine("emptySlowly");
            fillAmount += Time.deltaTime / fillTime;
            shapekeys.SetBlendShapeWeight(0, fillAmount * 100);
            if (fillAmount >= 1)
            {
                fillAmount = 1;
                isFull = true;
                steam.SetActive(true);
                filledAction.Invoke();
                if (sounds.clip == fillSound)
                {
                    sounds.Stop();
                    sounds.clip = fullSound;
                    sounds.Play();
                }
            }
        }
        // reset position if falls out of world
        if (transform.position.y < -2)
        {
            rigid.isKinematic = true;
            transform.position = spawnPos;
            rigid.isKinematic = false;
        }
    }

    public void delayedPhysicsIgnore(PlayerHand hand)
    {
        StartCoroutine("delayedPhysicsIgnoreCountdown",hand);
    }
    public void stopDelayedPhysicsIgnore()
    {
        StopCoroutine("delayedLayerReturnCountdown");
    }


    IEnumerator delayedPhysicsIgnoreCountdown(PlayerHand hand)
    {
        yield return new WaitForSeconds(.25F);
        hand.togglePhysicsRelation(colliders,false);
    }

}
