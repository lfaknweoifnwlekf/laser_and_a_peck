﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Valve.VR.InteractionSystem;
using UnityEngine.SceneManagement;

using UnityEngine.Events;

public class Player : MonoBehaviour {

    [SerializeField] public float hp = .3F;
    [SerializeField] public float maxhp;
    [SerializeField] public float regenTime = 1; // seconds till at max hp again
    [Range(0.01F,200)] public float rotateSpeed = 30; // Multiplier
    [Range(1000,30000)] public float speedDamageMultiplier = 300;

    [SerializeField] private UIStuff ui;

    [SerializeField] private AudioClip[] playlist;
    [SerializeField] private AudioClip deathMusic;
    [SerializeField] private AudioClip victoryMusic;
    [SerializeField] private AudioSource musicSource;
    private int currentSong = -1;
    [HideInInspector] public LevelStuff levelStuff;
    [SerializeField] private GameObject levelStuffPrefab;
    [HideInInspector] public float volumeMaster = 1;
    public float volumeSound = 1;
    public float volumeMusic = .5F;
    [SerializeField] private GameObject MenuStuff;
    [HideInInspector] public bool locomotionEnabled = true;


    public PlayerHand leftHand;
    public PlayerHand rightHand;

    public fullBodyAssign fbtMgr;


    public bool isDead = false;

    private bool levelJustLoaded = false; // hotfix to prevent loop of reloading level after first spawn
    private Color levelAmbientColor = Color.white;

    public enum sliderSettings
    {
        volume = 0,
        music = 1
    }


    [Tooltip("On Victory")] [SerializeField] UnityEvent victoryActions = new UnityEvent();
    [Tooltip("On Death")] [SerializeField] UnityEvent deathActions = new UnityEvent();
    [Tooltip("On Respawn")] [SerializeField] UnityEvent respawnActions = new UnityEvent();
    [Tooltip("On Level Load")] [SerializeField] UnityEvent loadLevelActions = new UnityEvent();

    // Use this for initialization
    void Start () {
        fbtMgr = Object.FindObjectOfType<fullBodyAssign>();
        ui.unfadeTimer = regenTime;
        ui.maxhp = maxhp;
        maxhp = hp;
        musicSource = GetComponent<AudioSource>();

        DontDestroyOnLoad(transform.parent.gameObject); // keep VR/player thing
        transform.parent.Find("RespawnZones").transform.parent = null; // prevent respawnzones from being impacted by locomotion
        transform.parent.Find("MainMenu").transform.parent = null; // prevent menu from being impacted by locomotion
        spawnLevelStuff();
        SceneManager.sceneLoaded += OnSceneLoaded;
    }


    // Update is called once per frame
    void Update () {
        // Music Player
        if (!musicSource.isPlaying) { //play random song if last song is over, will not repeat same song
            int randomSong = Random.Range(0, playlist.Length);
            while (randomSong == currentSong && playlist.Length > 1) { // no repeat songs
                randomSong = Random.Range(0, playlist.Length);
            }
            //Debug.Log("Random Song: " + randomSong +","+playlist.Length);
            musicSource.clip = playlist[randomSong];
            currentSong = randomSong;
            musicSource.Play();
        }
        // respawn
        //if(isDead && )
    }
    private void FixedUpdate() {
        // Regen HP
        if (hp < maxhp && !isDead) {
            hp += (maxhp / regenTime) * Time.deltaTime;
            if (hp > maxhp) {
                hp = maxhp;
            }
        }
    }


    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        spawnLevelStuff();

        levelAmbientColor = RenderSettings.ambientLight;
        respawn(false);
        levelJustLoaded = true;
        if (scene.buildIndex > 1)
        {
            die(false);
            RenderSettings.ambientLight = Color.blue;
        }
        loadLevelActions.Invoke();
    }

    private void spawnLevelStuff()
    {
        GameObject g = Instantiate(levelStuffPrefab, Vector3.zero, Quaternion.identity);
        SceneManager.MoveGameObjectToScene(g, SceneManager.GetActiveScene());
        levelStuff = g.GetComponent<LevelStuff>();
    }

    public void applyDamage(float amount) {
        if (!isDead) {
            hp = Mathf.Max(0, hp-amount);
            // red eye visuals
            if (hp > 0) {
                ui.setDamage(maxhp - hp);
            }
            else{
                // die
                die();
            }
        }
    }

    public void die(bool hardDeath = true) {
        Debug.Log("player is dead, hardDeath: " +hardDeath);
        isDead = true;
        rightHand.isDead = true;
        leftHand.isDead = true;
        leftHand.releaseAll();
        rightHand.releaseAll();
        fbtMgr.toggleCollission(false);
        if (hardDeath)
        {
            ui.die();
            musicSource.clip = deathMusic;
            musicSource.Play();
            RenderSettings.ambientLight = new Color(.9F, .25F, .2F);
        }
        deathActions.Invoke();
        // fbtMgr.applyFBTOffsets(); // don't overlap with respawnzones (shouldnt happen)
    }

    public void victory() {
        // play victory music
        if(musicSource.clip != victoryMusic)
        {
            musicSource.clip = victoryMusic;
            musicSource.Play();
        }
        foreach(Laser las in Object.FindObjectsOfType<Laser>())
        {
            las.turnOff();
        }
        victoryActions.Invoke();
    }

    public void loadLevel(int id)
    {
        if (id == 0) id = 1;// fix since 0 is a hotfix
        leftHand.releaseAll();
        rightHand.releaseAll();
        if (id == 1) MenuStuff.SetActive(true);
        else {  
            MenuStuff.SetActive(false);
            fbtMgr.applyFBTOffsets(); // force FBT to apply for convenience leaving menu
        }
        StartCoroutine(loadLevelDelayed(id, .3F));
    }

    IEnumerator loadLevelDelayed(int id, float duration)
    {
        yield return new WaitForEndOfFrame();
        yield return new WaitForSeconds(duration);
        SceneManager.LoadScene(id);
        yield return new WaitForEndOfFrame();
    }

    public void loadNextLevel()
    {
        if(SceneManager.GetActiveScene().buildIndex <= SceneManager.sceneCountInBuildSettings) loadLevel(SceneManager.GetActiveScene().buildIndex + 1);
    }
    
    public void respawn(bool forceLoad = true) {
        if(forceLoad && !levelJustLoaded) SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        if(levelJustLoaded) RenderSettings.ambientLight = levelAmbientColor;
        levelJustLoaded = false;
        isDead = false;
        rightHand.isDead = false;
        leftHand.isDead = false;
        ui.unDie();
        respawnActions.Invoke();
        fbtMgr.toggleCollission(true);
        if (AudioClip.ReferenceEquals(musicSource.clip, deathMusic) || AudioClip.ReferenceEquals(musicSource.clip, victoryMusic)) musicSource.Stop();
    }


    public void toggleLocomotion(bool on)
    {
        locomotionEnabled = on;

    }

    public void sliderChange(float amount, sliderSettings setting)
    {
        if (setting == sliderSettings.volume) setVolumeSound(amount);
        else if (setting == sliderSettings.music) setVolumeMusic(amount);
    }

    public void setVolumeSound(float amount)
    {
        volumeMaster = amount;
        AudioListener.volume = volumeMaster;
    }

    public void setVolumeMusic(float amount)
    {
       volumeMusic = amount;
        musicSource.volume = volumeMusic;
    }
}
