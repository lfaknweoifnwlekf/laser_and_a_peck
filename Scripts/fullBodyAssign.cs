﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;
using UnityEngine.Events;

/* Tries to figure out which full-body tracker is on which part of body (feet, hips) */

public class fullBodyAssign : MonoBehaviour
{
    public bool autoShuffleTrackers = true;

    [Header("Settings")]
    public bool footLocomotion = false;

    [Header("Trackers")]
    [SerializeField] private GameObject[] playerBits = new GameObject[3];
    [SerializeField] private Transform[] allPlayerBitTransforms = new Transform[6];
    private Vector3[] allPlayerBitTransformStartingScales = new Vector3[6];
    [SerializeField] private Zone[] zoneTesters = new Zone[3];
    private GameObject[] assignedTrackers = new GameObject[3];
    private Vector3[] playerBitStartLocs = new Vector3[3];
    private Quaternion[] playerBitStartRots = new Quaternion[3];
    private Vector3 startPos;
    private Quaternion startRot;
    private Vector3 startScale;
    [SerializeField] private Transform testTrackerContainer;
    [SerializeField] private Transform zonesContainer;
    [SerializeField] private Transform[] hands;
    private SteamVR_TrackedObject[] testTrackers;
    private Player player;
    
    [SerializeField] private Transform head;
    [SerializeField] private Transform[] headZones;



    // Start is called before the first frame update
    void Start()
    {
        testTrackers =  testTrackerContainer.GetComponentsInChildren<SteamVR_TrackedObject>();
        player = GetComponentInChildren<Player>();
        for (int i = 0; i < playerBits.Length; i++)
        {
            playerBitStartLocs[i] = playerBits[i].transform.position;
            playerBitStartRots[i] = playerBits[i].transform.rotation;
        }

        for (int i = 0; i < allPlayerBitTransforms.Length; i++)
        {
            allPlayerBitTransformStartingScales[i] = allPlayerBitTransforms[i].localScale;
        }
        startPos = transform.position;
        startRot = transform.rotation;
        startScale = transform.localScale;
    }

    public void toggleCollission(bool enabled)
    {
        int newLayer;
        if (enabled) newLayer = 10;
        else newLayer = 17;
        foreach(Transform n in allPlayerBitTransforms)
        {
            foreach(Collider c in n.GetComponentsInChildren<Collider>())
            {
                c.gameObject.layer = newLayer;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (autoShuffleTrackers)
        {
            // remove hand controllers from tracked object list
            foreach (SteamVR_TrackedObject tracker in testTrackers)
            {
                if (Vector3.Distance(tracker.transform.position, hands[0].transform.position) < .05F || Vector3.Distance(tracker.transform.position, hands[1].transform.position) < .05F)
                {
                    if (tracker.GetComponent<Collider>().enabled) { 
                        foreach (Zone z in zoneTesters)
                        {
                            foreach (GameObject obj in z.containedObjects)
                            {
                                if (GameObject.ReferenceEquals(obj, tracker.gameObject)) z.applyExit(obj);
                            }
                        }
                        tracker.GetComponent<Collider>().enabled = false;
                        tracker.GetComponent<Renderer>().enabled = false;
                    }
                }
                else
                {
                    if (!tracker.GetComponent<Collider>().enabled)
                    {
                        tracker.GetComponent<Collider>().enabled = true;
                        tracker.GetComponent<Renderer>().enabled = true;
                    }
                }
            }

            // align hips to player height
            if(zoneTesters[2].containedObjects.Count > 0)
            {
                playerBits[2].transform.position = new Vector3(playerBits[2].transform.position.x, zoneTesters[2].containedObjects[0].transform.position.y - .1F, playerBits[2].transform.position.z);
            }
        }

    }

    public void resetFBTOffsets()
    {
        // Reset playspace
        transform.position = startPos;
        transform.rotation = startRot;
        transform.localScale = new Vector3(1, 1, 1);

        testTrackerContainer.gameObject.SetActive(true);
        for (int i=0;i< playerBits.Length;i++)
        {
            playerBits[i].transform.parent = transform;
            playerBits[i].transform.position = playerBitStartLocs[i];
            playerBits[i].transform.rotation = playerBitStartRots[i];
            playerBits[i].GetComponent<PlayerBit>().toggleVisual(true);
            if (assignedTrackers[i] != null)
            {
                assignedTrackers[i].GetComponent<Renderer>().enabled = true;
                assignedTrackers[i].transform.parent = testTrackerContainer.transform;
                assignedTrackers[i] = null;
            }
        }
        // reset scale of bits
        for (int i = 0; i < allPlayerBitTransforms.Length; i++)
        {
            allPlayerBitTransforms[i].localScale = allPlayerBitTransformStartingScales[i];
        }
        autoShuffleTrackers = true;
    }

    public void applyFBTOffsets()
    {
        if (autoShuffleTrackers)
        {
            autoShuffleTrackers = false;

            for (int i = 0; i < playerBits.Length; i++)
            {
                //Debug.Log("bit: " + i + " has " + zoneTesters[i].containedObjects.Count + " trackers.");
                if (zoneTesters[i].containedObjects.Count > 0)
                {
                    GameObject tracker = zoneTesters[i].containedObjects[0];
                    GameObject bit = playerBits[i];
                    //bit.transform.parent = playspace;
                    // closestTracker.transform.SetPositionAndRotation(trackerTest.transform.position, trackerTest.transform.rotation);
                    // tracker.GetComponent<SteamVR_TrackedObject>().SetDeviceIndex((int)trackerTest.index);
                    tracker.transform.parent = transform;
                    bit.transform.parent = tracker.transform;
                    assignedTrackers[i] = tracker;
                    bit.GetComponent<PlayerBit>().toggleVisual(true);
                    tracker.GetComponent<Renderer>().enabled = false;
                }
                else
                {
                    playerBits[i].GetComponent<PlayerBit>().toggleVisual(false);
                }

            }
            // scale player to standardized height based on head height (so short players can  reach blocks placed by tall players etc)
            if (head.transform.localPosition.y > 1 && head.transform.localPosition.y < 2.5F)
            {
                float ratio = 1.7F / head.transform.localPosition.y;
                transform.localScale *= ratio;
                for (int i = 0; i < allPlayerBitTransforms.Length; i++)
                {
                    allPlayerBitTransforms[i].localScale = (1/ratio) * allPlayerBitTransformStartingScales[i];
                }
            }
            // cleanup
            testTrackerContainer.gameObject.SetActive(false);
            zonesContainer.gameObject.SetActive(false);

            // put head triggered zones at player's real head height
            foreach (Transform t in headZones)
            {
                t.position = new Vector3(t.position.x, head.localPosition.y,t.position.z);
            }
        }
    }
}
