﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserVisual : MonoBehaviour
{

    [SerializeField] private GameObject headSound;
    private Player player;
    private GameObject headSoundObj;

    // Start is called before the first frame update
    void Start()
    {
        player = Object.FindObjectOfType<Player>();
    }

    //head sound thing
    private void OnTriggerStay(Collider other)
    {
        if (!player.isDead)
        {
            // find point on laser that is closest to head (sohCAHtoa)
            float angle = Mathf.Abs(Vector3.Angle(transform.forward, (other.transform.position - transform.position)));
            float distance = Mathf.Abs(Vector3.Distance(transform.position, other.transform.position));
            float adjacent = Mathf.Cos(Mathf.Deg2Rad * angle) * distance;

            Vector3 closestPoint = transform.position + transform.forward * adjacent;

            if (headSoundObj == null)
            {
                headSoundObj = Instantiate(headSound, closestPoint, transform.rotation) as GameObject;
            }
            else
            {
                headSoundObj.transform.position = closestPoint;
            }
        }
        else
        {
            if(headSoundObj != null)
            {
                headSoundObj.SetActive(false);
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        //Destroy(headSoundObj);
        //headSoundObj = null;
    }
}
