﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Interactable : MonoBehaviour {

    public bool active = false;
    private float lastActivation = 0;

    [SerializeField] UnityEvent onAction = new UnityEvent();
    [SerializeField] UnityEvent offAction = new UnityEvent();

    
    public void toggle() {
        if (Time.time - lastActivation > .1F)
        {
            lastActivation = Time.time;
            active = !active;
            if (active)
            {
                onAction.Invoke();
            }
            else
            {
                offAction.Invoke();
            }
        }
    }
}
