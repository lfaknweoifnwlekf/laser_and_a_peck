﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Collider))]
public class Zone : MonoBehaviour
{
    public bool activated = false;
    [Tooltip("disables detection and always reports as activated")] public bool activatedAlways = false;
    [Tooltip("activated by any object")] [SerializeField] private bool allowAny = false;
    [Tooltip("objects that can activate it")] [SerializeField] private GameObject[] targetObjects;
    private Color defaultColor;
    [SerializeField] private Color highlightColor;
    private Renderer rend;
    private Material mat;
    private ZoneGroup parent;


    [Tooltip("Action that happens when somethign enters this zone")] [SerializeField] UnityEvent triggerAction = new UnityEvent();
    [Tooltip("Action that happens when something exits this zone")] [SerializeField] UnityEvent exitAction = new UnityEvent();
    [Tooltip("Action that happens when this zone becomes empty")] [SerializeField] UnityEvent emptyAction = new UnityEvent();

    private AudioSource aud;
    [SerializeField] private AudioClip enterSound;
    [SerializeField] private AudioClip exitSound;

    public List<GameObject> containedObjects;

    private Player player;

    [SerializeField] bool debug = false;



    private void Start()
    {
        player = Object.FindObjectOfType<Player>();

        rend = GetComponent<Renderer>();
        if (rend != null)
        {
            mat = rend.material;
            defaultColor = mat.GetColor("_MainColor");
        }
        GetComponent<Collider>().isTrigger = true;
        parent = GetComponentInParent<ZoneGroup>();
        aud = GetComponent<AudioSource>();
        if (aud == null && (enterSound != null || exitSound != null))
        {
            aud = gameObject.AddComponent<AudioSource>();
            aud.spatialBlend = .95F;
            aud.minDistance = .1F;
            aud.maxDistance = 30;
        }

        // aud.spatialize = true;
    }

    private void OnEnable()
    {
        activated = false;
        containedObjects.Clear();
        if (rend != null) rend.enabled = !activatedAlways;
        if (mat != null) mat.SetColor("_MainColor", defaultColor);
    }

    private void OnTriggerEnter(Collider other)
    {        
        bool activate = allowAny;
        GameObject obj = other.GetComponentInParent<Rigidbody>().gameObject;
        if (allowAny || testTarget(obj,true))
        {
            containedObjects.Add(obj);
            activated = true;
            if(mat != null) mat.SetColor("_MainColor",highlightColor);
            if (parent != null) parent.trigger();
            if (debug) Debug.Log("ENTER: " + other.name);
            triggerAction.Invoke();
            if (enterSound != null) aud.PlayOneShot(enterSound);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        GameObject obj = other.GetComponentInParent<Rigidbody>().gameObject;
        applyExit(obj);

    }

    public void applyExit(GameObject obj)
    {
        if (allowAny || testTarget(obj, false))
        {
            if (debug) Debug.Log("EXIT: " + obj.name);
            containedObjects.Remove(obj);
            exitAction.Invoke();
            if (containedObjects.Count == 0)
            {
                activated = false;
                if (mat != null) mat.SetColor("_MainColor", defaultColor);
                if (exitSound != null) aud.PlayOneShot(exitSound);
                else if (aud != null) aud.Stop();
                if (parent != null) parent.trigger();
                emptyAction.Invoke();
            }
        }
    }

    private bool testTarget(GameObject other,bool add)
    {
        foreach (GameObject target in targetObjects)
        {
            if (GameObject.ReferenceEquals(other, target))
            {
                if ((add && !containedObjects.Contains(other)) || (!add && containedObjects.Contains(other))) return true;
            }
        }
        return false;
    }

    public void setActivatedAlways(bool k)
    {
        activatedAlways = k;
        activated = k;
        GetComponent<Renderer>().enabled = !k;
    }
}
