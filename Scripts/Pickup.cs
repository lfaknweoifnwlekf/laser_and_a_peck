﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour {

    public float hp = .8F;
    private float maxhp;
    public AudioClip[] clinkSounds;
    private AudioSource audion;
    private Player player;
    private Vector3 spawnPos;
    private Rigidbody rigid;

    private bool breakable = false;

    private Collider[] colliders;

    private void OnCollisionEnter(Collision collision) {
        if (audion != null && collision.transform.tag != "Player")
        {
            audion.clip = clinkSounds[Random.Range(0, clinkSounds.Length - 1)];
            audion.Play();
        }
        if (breakable) {
            // glass and shit breaks idk
        }
    }

    // Use this for initialization
    void Start () {
        player = Object.FindObjectOfType<Player>();
        audion = GetComponent<AudioSource>();
        if (audion != null) audion.volume = .2F;
        maxhp = hp;
        if (transform.tag == "pickupabledestructable" && hp > 0) {
            breakable = true;
        }
        colliders = GetComponentsInChildren<Collider>();
        rigid = GetComponent<Rigidbody>();
        spawnPos = transform.position + new Vector3(0,.15F,0);
    }


    public bool beingDamaged(float amount) {
        hp -= amount;
        if (hp <= 0) {
            return false;
        }
        else return true;
    }

    public bool isDead() {
        if (hp <= 0) {
            return true;
        }
        else return false;
    }

    // Update is called once per frame
    void Update () {

        // reset position if falls out of world
        if (transform.position.y < -2)
        {
            rigid.isKinematic = true;
            transform.position = spawnPos;
            rigid.isKinematic = false;
        }
    }

    public void delayedPhysicsIgnore(PlayerHand hand)
    {
        StartCoroutine("delayedPhysicsIgnoreCountdown", hand);
    }
    public void stopDelayedPhysicsIgnore()
    {
        StopCoroutine("delayedLayerReturnCountdown");
    }


    IEnumerator delayedPhysicsIgnoreCountdown(PlayerHand hand)
    {
        yield return new WaitForSeconds(.25F);
        hand.togglePhysicsRelation(colliders, false);
    }
}
