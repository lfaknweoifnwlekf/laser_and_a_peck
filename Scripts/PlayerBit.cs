﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody))]
public class PlayerBit : MonoBehaviour
{
    [Header("Just activates when enabled, if it is indeed a tracker and not a redundant hand/head")]
    private Player player;
    [SerializeField] private GameObject disappearEffect;
    private Material mat; // skin
    [SerializeField] private Material killMat;
    private Color defaultSkinColor;
    private Color burntSkinColor;
    private float lastHPVisual = 0;
    [SerializeField] private PlayerHand thisHand;
    private Vector3 posLastFrame;
    private Vector3 posDamageStarted;
    private float lastDamageTimestamp = 0;
    private float currentSpeed = 0;
    [SerializeField] private Renderer rend;
    [SerializeField] private Transform mainTransform;

    private Vector3 defaultScale;

    // Start is called before the first frame update
    void Start()
    {
        player = Object.FindObjectOfType<Player>();

        if(rend == null) rend = GetComponentInChildren<Renderer>();
        foreach(Material m in rend.materials)
        {
            if (m.name == "skin (Instance)") mat = m;
        }
        defaultSkinColor = mat.color;
        burntSkinColor = Color.red;
        if(thisHand == null) thisHand = GetComponent<PlayerHand>();
        if (mainTransform == null) mainTransform = transform;
        defaultScale = mainTransform.localScale;
    }


    public void beingDamaged(float amount)
    {
        if (Time.time - lastDamageTimestamp > .1F) posDamageStarted = transform.position;
        lastDamageTimestamp = Time.time;
        // distance from initial damage point method
        player.applyDamage(amount + amount * Vector3.Distance(transform.position,posDamageStarted)  * currentSpeed* player.speedDamageMultiplier);
        // default damage method
        // player.applyDamage(amount);
        // plain movement damage method
        // player.applyDamage(amount + amount * currentSpeed * player.speedDamageMultiplier);
        lastHPVisual = Mathf.Max( (player.maxhp - player.hp) / player.maxhp,0);
        applyDamageVisual(lastHPVisual);
        if(thisHand!=null) thisHand.doHaptic(.001F, 280, player.hp/ player.maxhp);
    }

    public void toggleVisual(bool show)
    {        
        if (disappearEffect != null) Instantiate(disappearEffect,transform.position, transform.rotation);
        gameObject.SetActive(show);
    }

    private void Update()
    {
        // Burn skin effects (global reduction, local addition)
        if(lastHPVisual > (player.maxhp - player.hp) / player.maxhp || player.isDead)
        {
            lastHPVisual = (player.maxhp - player.hp) / player.maxhp;
            applyDamageVisual(lastHPVisual);
        }

        // determine speed (for damage calculations)
        currentSpeed = Vector3.Distance(transform.position, posLastFrame);
        posLastFrame = transform.position;
    }

    private void applyDamageVisual(float amount)
    {
        mat.SetFloat("_DetailNormalMapScale", -amount);
        mat.SetColor("_Color", Color.Lerp(defaultSkinColor, burntSkinColor, amount));
    }

    public void createKillVisual()
    {
        Debug.Log("Creating kill visual for "+gameObject.name);
        GameObject killVisual = new GameObject("killVisual");
        killVisual.transform.SetPositionAndRotation(rend.transform.position, rend.transform.rotation);
        killVisual.transform.parent = player.levelStuff.transform;
        killVisual.transform.localScale = rend.transform.lossyScale;
        MeshFilter meshfilter =  killVisual.AddComponent<MeshFilter>();
        MeshRenderer meshrenderer =  killVisual.AddComponent<MeshRenderer>();
        if(rend.GetType() == typeof(SkinnedMeshRenderer)){
            meshfilter.mesh = (rend as SkinnedMeshRenderer).sharedMesh;
        }
        else {
            meshfilter.mesh = rend.GetComponent<MeshFilter>().mesh;
        }
        Material[] matChange = rend.materials;

        for (int i=0; i<matChange.Length;i++)
        {
            matChange[i] = killMat;
        }
        meshrenderer.materials = matChange;
    }

    public void applyScale(float ratio)
    {
        mainTransform.localScale = defaultScale * (1 / ratio); // reverse scale to compensate for playspace growing (otherwise short people would have giant hands)
    }

    public void resetScale()
    {
        mainTransform.localScale = defaultScale;
    }
}
