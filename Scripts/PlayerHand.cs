﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class PlayerHand : MonoBehaviour {

    [SerializeField] private Zone grabZone;
    private Player player;
    private float hp;
    private float maxhp;
    private float regenTime; // seconds till at max hp again
    public Shader outlineShader;
    [SerializeField] private float throwForce = 80;

    [HideInInspector] public bool hasObjectInHand = false;
    private GameObject objectInHand;
    private GameObject highlightedObject;
    private Shader tmpShader;
    private Renderer tmpRenderer;

    private bool isInteracting = false;
    private Interactable currentInteractable;
    private Rigidbody rigid;
    private Vector3 posLastFrame;
    private Vector3 globalPosLastFrame;

    public bool isMovingPlayer = false;
    public bool isRotatingPlayer = false;
    private float rotatePlayerAngleLastFrame = 0;

    private Collider[] colliders;

    public Animator handAnimat;

    [HideInInspector] public bool isDead =false;

    [SerializeField] private PlayerHand otherHand;

    public UIStuff ui;

    // Controller input setup
    protected SteamVR_Behaviour_Pose trackedObj;
    private SteamVR_Action_Boolean grab = SteamVR_Input.GetAction<SteamVR_Action_Boolean>("GrabGrip");
    private SteamVR_Action_Boolean move = SteamVR_Input.GetAction<SteamVR_Action_Boolean>("Trigger");
    private SteamVR_Action_Boolean menu = SteamVR_Input.GetAction<SteamVR_Action_Boolean>("Menu");
    [SerializeField] private SteamVR_Action_Vibration haptic;

    private void OnCollisionEnter(Collision collision)
    {
        // drop if touching block object
        if(collision.gameObject.layer == 16)
        {
            Debug.Log("disallowplayer");
            releaseAll();
            doHaptic(.02F, 200, .3F);
        }
    }


    void Awake() {
        trackedObj = GetComponent<SteamVR_Behaviour_Pose>();
        rigid = GetComponent<Rigidbody>();
        posLastFrame = transform.position;
    }

    public void refreshGrabHighlight()
    {
        if (!hasObjectInHand && !isDead)
        {
            if (grabZone.containedObjects.Count > 0)
            {
                GameObject obj = grabZone.containedObjects[grabZone.containedObjects.Count - 1]; // get last (most recent entry)
                if (!GameObject.ReferenceEquals(highlightedObject, obj))
                {
                    clearGrabHighlight();
                    // apply outline shader                
                    Debug.Log("highlighted " + obj.name);
                    tmpRenderer = obj.GetComponent<Renderer>();
                    tmpShader = tmpRenderer.material.shader;
                    tmpRenderer.material.shader = outlineShader;
                    highlightedObject = obj;
                    doHaptic(.001F, 50, .1F);

                    // prevent other hand from also holding it
                    if (GameObject.ReferenceEquals(obj, otherHand.highlightedObject))
                    {
                        otherHand.clearGrabHighlight();
                    }
                }
                else
                {
                    Debug.Log("Ignore Repeat highlight: " + obj.name);
                }
            }
        }
    }
    public void clearGrabHighlight()
    {
        if (tmpRenderer != null)
        {
            tmpRenderer.material.shader = tmpShader;
            tmpRenderer = null;
            tmpShader = null;
        }
        // Debug.Log("Clear grab  " + highlightedObject.name);
        highlightedObject = null;
    }

    // Force all objects to be dropped by this hand
    public void releaseAll(bool remainPickedUp = false)
    {
        if (hasObjectInHand)
        {
            hasObjectInHand = false;
            Rigidbody other = objectInHand.GetComponent<Rigidbody>();

            if (!remainPickedUp)
            {
                objectInHand.transform.parent = player.levelStuff.transform; // ensure it is in the loaded scene, not the DontDestroy scene
                 //other.gameObject.layer = 12; // return to pickup layer
                other.SendMessage("delayedPhysicsIgnore", this);
                other.isKinematic = false;
                other.useGravity = true;
                Vector3 handVel = transform.position - globalPosLastFrame;
                other.velocity = (handVel) * throwForce; // throw
                doHaptic(.01F, 200, .2F);
            }
            objectInHand = null;
        }
        clearGrabHighlight();
    }

    // Haptics
    public void doHaptic(float duration, float frequency, float amp)
    {
        haptic.Execute(0, duration, frequency, amp, trackedObj.inputSource);
    }

    // Delayed replacement of physics of previously dropped items
    public void togglePhysicsRelation(Collider[] otherColliders, bool disabled)
    {
        foreach(Collider ColA in colliders)
        {
            foreach(Collider ColB in otherColliders)
            {
                Physics.IgnoreCollision(ColA, ColB, disabled);
            }
        }
    }

    // Use this for initialization
    void Start () {
        player = Object.FindObjectOfType<Player>();

        hp = player.hp;
        maxhp = hp;
        regenTime = player.regenTime;
        colliders = GetComponentsInChildren<Collider>();
    }

    // Update is called once per frame
    void Update()
    {   
        // Release
        if (grab.GetStateUp(trackedObj.inputSource) || isDead) {
            // squeeze animation
            handAnimat.SetBool("grip", false);
            // cancel pickup
            if (hasObjectInHand) {
                releaseAll();
            }
            // cancel interaction
            if (isInteracting) {
                isInteracting = false;
                currentInteractable.enabled = false;
                currentInteractable = null;
            }
        }
        // Grab
        if (grab.GetStateDown(trackedObj.inputSource)) {
            // squeeze animation
            handAnimat.SetBool("grip", true);
            // item / interact
            if(!isDead && highlightedObject != null) {
                if (highlightedObject.transform.tag == "interactable")
                {
                    highlightedObject.GetComponent<Interactable>().toggle();
                }
                else if (highlightedObject.layer == 12 || highlightedObject.layer == 13)
                {
                    // prevent unwanted layer change
                    highlightedObject.SendMessage("stopDelayedPhysicsIgnore");
                    // set reference
                    hasObjectInHand = true;
                    objectInHand = highlightedObject;
                    // create constraint
                    Collider[] otherColliders = highlightedObject.GetComponentsInChildren<Collider>();
                    togglePhysicsRelation(otherColliders, true); // TODO: Optimize this (gotta merge pickup/coffeemug classes so i can get the variable
                    otherHand.togglePhysicsRelation(otherColliders, false); // TODO: Optimize this (gotta merge pickup/coffeemug classes so i can get the variable
                    highlightedObject.GetComponent<Rigidbody>().isKinematic = true;
                    highlightedObject.GetComponent<Rigidbody>().useGravity = false;
                    highlightedObject.transform.parent = transform;
                    // release it from other hand (if applicable)
                    if (GameObject.ReferenceEquals(highlightedObject, otherHand.objectInHand))
                    {
                        otherHand.releaseAll(true);
                    }
                    clearGrabHighlight();
                }
            }
        }
        // Move 
        if (move.GetState(trackedObj.inputSource) && player.locomotionEnabled)
        {
            if (otherHand.isMovingPlayer)
            {
                if (!isRotatingPlayer)
                {
                    rotatePlayerAngleLastFrame = Vector3.Distance(transform.localPosition, otherHand.transform.localPosition);
                    isRotatingPlayer = true;
                }
                player.fbtMgr.transform.RotateAround(otherHand.transform.position, new Vector3(0,1,0) , (rotatePlayerAngleLastFrame - Vector3.Distance(transform.localPosition, otherHand.transform.localPosition)) *player.rotateSpeed);
                rotatePlayerAngleLastFrame = Vector3.Distance(transform.localPosition, otherHand.transform.localPosition);
            }
            else if(!otherHand.isRotatingPlayer)
            {
                isMovingPlayer = true;
                player.fbtMgr.transform.position += Quaternion.Euler(0, player.fbtMgr.transform.rotation.eulerAngles.y, 0) * (posLastFrame - transform.localPosition);
            }
        }
        // Move end
        if (move.GetStateUp(trackedObj.inputSource))
        {
            isMovingPlayer = false;
            isRotatingPlayer = false;
        }
        // back to menu
        if (menu.GetStateDown(trackedObj.inputSource))
        {
            player.loadLevel(1);
        }

        posLastFrame = transform.localPosition;
        globalPosLastFrame = transform.position;
    }


    private void FixedUpdate() {
        // Regen HP
        if(hp < maxhp && !isDead) {
            hp += (maxhp / regenTime) * Time.deltaTime;
            if(hp > maxhp) {
                hp = maxhp;
            }
        }
    }
}
