﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour {

    public bool isEnabled;
    private bool wasEnabled;
    private bool isMoving = false;
    private bool isWarmingUp = false;
    private bool isKillVisual = false;
    [SerializeField] private bool skipWarmUp = false;

    private float warmupThickness = 4;
    private float warmupTime = .8F;
    private int warmupFlickerCount = 2;

    public LayerMask mask;
    public LayerMask playerOnlyMask;

    
    [SerializeField] private GameObject laserMeshPrefab;
    [SerializeField] private GameObject childLaserPrefab;
    private Laser childLaser;
    [HideInInspector] public int childIndex = 0; // how many parent lasers there are
    private int maxChildren = 40; // max amount of reflected lasers (e.g. when you point 2 mirrors at themselves which every player will do
    private GameObject lasermesh;
    public Material warmupMat;
    public Material laserMat;
    public Material killMat;
    private GameObject warmupmesh;
    public GameObject sizzleSound;
    private GameObject sizzler;
    public GameObject smokeEffect;
    private GameObject smoker;
    public GameObject fireEffect;
    private AudioSource aud;

    private CapsuleCollider headSoundTrigger;
    [SerializeField] AudioClip laserHitsHead;

    private int damage = 1; // per second
    private int pushAmount = 1; // units of force per second
    private Vector3 laserStartScale;

    private Player player;
    //private Vector3 laserDir = Transform.forward;

    private RaycastHit hit;

	// Use this for initialization
	void Start () {
        wasEnabled = isEnabled;

        player = Object.FindObjectOfType<Player>();
        aud = GetComponent<AudioSource>();
        // create/find lasermesh
        if(transform.childCount > 0) lasermesh = GetComponentInChildren<LaserVisual>().gameObject;
        else 
        {
            lasermesh = Instantiate(laserMeshPrefab, transform.position, transform.rotation);
            lasermesh.transform.parent = transform;
            lasermesh.GetComponent<Renderer>().material = laserMat;
            //Debug.Log("Created lasermesh for " + transform.name);
        }
        laserStartScale = lasermesh.transform.localScale;
        // trigger
        headSoundTrigger = GetComponentInChildren<CapsuleCollider>();
        // disable lasermesh
        if (!isEnabled) {
            lasermesh.SetActive(false);
            headSoundTrigger.enabled = false;
        }
        if (!skipWarmUp && warmupmesh == null)
        {
            // create warmupmeshviusal
            warmupmesh = Instantiate(laserMeshPrefab, lasermesh.transform.position, lasermesh.transform.rotation) as GameObject;
            warmupmesh.name = "warmupmesh";
            warmupmesh.transform.parent = transform;
            warmupmesh.GetComponent<Renderer>().material = warmupMat;
            warmupmesh.GetComponent<Collider>().enabled = false;
        }        
        // force warmup on first spawn
        if (isEnabled)
        {
            
            turnOn();
        }
    }

    public void turnOn() {
        if (skipWarmUp) turnOnConcludes();
        else StartCoroutine(warmup());
        //Debug.Log("Turning on laser");
    }

    private void turnOnConcludes()
    {
        // enable laser
        lasermesh.SetActive(true);
        wasEnabled = true;
        isEnabled = true;
        headSoundTrigger.enabled = true;
        isWarmingUp = false;
    }


    public void turnOff()
    {
        StopCoroutine("warmup");
        isEnabled = false;
        wasEnabled = false;
        isWarmingUp = false;
        lasermesh.SetActive(false);
        headSoundTrigger.enabled = false;
        aud.Stop();
        // turn off child laser chain
        if (childLaser != null) childLaser.turnOff();
        ///Debug.Log("Turning off laser");
    }



    IEnumerator warmup() {
        isEnabled = true;
        wasEnabled = true;
        isWarmingUp = true;
        aud.Play();
        // warmup visuals
        for (int i = 0; i < warmupFlickerCount; i++)
        {
            warmupmesh.SetActive(true);
            yield return new WaitForSeconds(warmupTime / warmupFlickerCount / 2);
            warmupmesh.SetActive(false);
            yield return new WaitForSeconds(warmupTime / warmupFlickerCount / 2);
        }
        if (isWarmingUp)
        {
            turnOnConcludes();
        }
    }

    // Update is called once per frame
    void Update () {
        if (isEnabled && wasEnabled && !isKillVisual) {
            Vector3 laserDir = transform.forward;
            if (Physics.Raycast(transform.position, laserDir, out hit, Mathf.Infinity, mask)) {
                // scale/place laser mesh to match size
                float distance = Vector3.Distance(transform.position, hit.point);              
                // warmup effects only
                if (isWarmingUp) {
                    warmupmesh.transform.localScale = new Vector3(warmupThickness, warmupThickness, distance * 100);
                }
                // Full laser action
                else if(!isWarmingUp){
                    lasermesh.transform.localScale = new Vector3(laserStartScale.x, laserStartScale.y, distance * 100);
                    //Debug.DrawLine(transform.position, hit.point, Color.cyan);

                    // effect hit objects
                    if (hit.transform.tag == "Player" || hit.transform.tag == "pickupabledestructable" || hit.transform.tag == "MainCamera") {
                        // fire particles (first death)
                        if (hit.transform.tag == "Player" && !player.isDead && player.hp <= .07F) Instantiate(fireEffect, hit.point, Quaternion.identity);

                        // do damage
                        bool playerIsAlive = !player.isDead;
                        hit.transform.SendMessage("beingDamaged", damage * Time.deltaTime);
                        if (hit.transform.tag == "Player" && playerIsAlive && player.isDead)
                        {
                            enableKillVisual();
                            hit.transform.SendMessage("createKillVisual");
                        }
                        //Debug.Log("damaging target with laser: " + damage + ", " + hit.transform.name + ", " + hit.transform.gameObject.tag);

                        // other particles and effects
                        if (hit.transform.tag == "Player" && !player.isDead) {
                            if (hit.transform.GetComponent<AudioListener>() != null)
                            {
                                AudioSource.PlayClipAtPoint(laserHitsHead, hit.point,.1F);
                            }
                            if (sizzler == null) {
                                sizzler = Instantiate(sizzleSound, hit.point, Quaternion.Euler( hit.normal)) as GameObject;
                            }
                            else {
                                sizzler.SetActive(false);
                                sizzler.transform.SetPositionAndRotation(hit.point, Quaternion.Euler(hit.normal));
                                sizzler.SetActive(true);
                            }
                        }
                    }
                    // Mirror Reflection
                    if (hit.transform.tag == "reflect" && childIndex < maxChildren)
                    {
                        if (childLaser == null)
                        {
                            childLaser = (Instantiate(childLaserPrefab, hit.point + hit.normal / 300, transform.rotation * Quaternion.Euler(hit.normal)) as GameObject).GetComponent<Laser>();
                            childLaser.childIndex = childIndex + 1;
                            childLaser.isEnabled = true;
                        }
                        childLaser.transform.LookAt(hit.point + Vector3.Reflect(hit.point - transform.position, hit.normal));
                        childLaser.transform.position = hit.point; // avoid spawning inside the reflection object
                        if (!childLaser.isEnabled) childLaser.turnOn();
                    }
                    else
                    {
                        if (childLaser != null) childLaser.turnOff();
                    }
                    // Force
                    if (hit.transform.tag == "pickupable" || hit.transform.tag == "pickupabledestructable") {
                        hit.transform.GetComponent<Rigidbody>().AddForce(pushAmount * Time.deltaTime * laserDir);
                    }
                    // Destroy/ smoke
                    if(hit.transform.tag == "pickupabledestructable")
                    {
                        if (smoker == null)
                        {
                            smoker = Instantiate(smokeEffect, hit.point, Quaternion.Euler(hit.normal)) as GameObject;
                        }
                        else
                        {
                            smoker.SetActive(false);
                            smoker.transform.SetPositionAndRotation(hit.point, Quaternion.Euler(hit.normal));
                            smoker.SetActive(true);                            
                        }
                        Pickup pickup = hit.transform.GetComponent<Pickup>();
                        if (pickup.isDead())
                        {
                            GameObject fire = Instantiate(fireEffect, hit.point, Quaternion.Euler(hit.normal)) as GameObject;
                            pickup.gameObject.SetActive(false);
                        }
                    }
                    // activate receivers
                    if(hit.transform.tag == "receiver")
                    {
                        hit.transform.SendMessage("signal");
                    }
                }
            }
            wasEnabled = true;
        }
        else if (!isEnabled && wasEnabled && !isWarmingUp && !isKillVisual) {
            turnOff();
            wasEnabled = false;
        }
        else if(isEnabled && !wasEnabled && !isWarmingUp && !isKillVisual) {
            turnOn();
            wasEnabled = true;
        }
    }

    private void enableKillVisual()
    {
        lasermesh.GetComponent<Renderer>().material = killMat;
        isKillVisual = true;
    }

    private void FixedUpdate() {
    }

}
