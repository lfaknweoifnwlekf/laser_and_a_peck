﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIStuff : MonoBehaviour {

    public Image redeye;
    public Image redscreen;
    public Image blackscreen;

    [HideInInspector] public float unfadeTimer; // seconds till fade away, same as Player regentime (set by player script)
    [HideInInspector] public float maxhp; // set by Player.cs
    [HideInInspector] public bool isDead = false;
    private float currentDamage = 0;
    public Animation deathanimation;


	// Use this for initialization
	void Start () {
        //deathanimation = GetComponent<Animation>();
	}

    public void setDamage(float amount) {
        //Debug.Log("setDamage: " + currentDamage + ", " + amount );
        if(currentDamage < amount) {
            currentDamage = amount;
            setDamageEffect(amount / maxhp);
        }
    }

    // max effect
    private void setDamageEffect(float ratio) {
        if (!isDead || ratio == 0) {
            redeye.color = new Color(redeye.color.r, redeye.color.g, redeye.color.b, ratio * 60 / 255);
            redscreen.color = new Color(redscreen.color.r, redscreen.color.g, redscreen.color.b, ratio * 25 / 255);
        }
    }

    private void setBlackscreen(float darkness)
    {
        blackscreen.color = new Color(blackscreen.color.r, blackscreen.color.g, blackscreen.color.b, darkness);
    }

    public void die() {
        isDead = true;
        setBlackscreen(.8F);
        setDamageEffect(0);
    }
    public void unDie() {
        isDead = false;
        setDamageEffect(0);
        setBlackscreen(0);

    }

    // Update is called once per frame
    void Update () {
        // fade away with time
        if (!isDead && currentDamage > 0) {
            currentDamage -= (maxhp / unfadeTimer) * Time.deltaTime;
            if(currentDamage < 0) {
                currentDamage = 0;
            }
            setDamageEffect(currentDamage / maxhp);
        }
    }
}
