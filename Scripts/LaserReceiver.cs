﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LaserReceiver : MonoBehaviour
{
    [SerializeField] UnityEvent onAction = new UnityEvent();
    [SerializeField] UnityEvent offAction = new UnityEvent();
    [SerializeField] AudioClip onSound;
    [SerializeField] AudioClip offSound;

    private bool isOn = false;
    private AudioSource audion;
    private GameObject onVisualObj;
    private float delayBeforeDeactivation = .1F;
    private float lastSignalTimestamp = 0;

    // Start is called before the first frame update
    void Start()
    {
        audion = GetComponent<AudioSource>();
        onVisualObj = transform.GetChild(0).gameObject;
    }

    private void Update()
    {
        // automatically disable if no signal was received recently
        if(isOn && Time.time - lastSignalTimestamp > delayBeforeDeactivation)
        {
            turnOff();
        }
    }

    public void signal()
    {
        lastSignalTimestamp = Time.time;
        if (!isOn)
        {
            turnOn();
        }
    }

    private void turnOn()
    {
        isOn = true;
        onAction.Invoke();
        audion.clip = onSound;
        audion.Play();
        onVisualObj.SetActive(true);
    }
    private void turnOff()
    {
        isOn = false;
        offAction.Invoke();
        audion.clip = offSound;
        audion.Play();
        onVisualObj.SetActive(false);
    }

}
