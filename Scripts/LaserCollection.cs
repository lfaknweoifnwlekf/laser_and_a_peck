﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Helper to toggle all descendent lasers at once
public class LaserCollection : MonoBehaviour
{
    Laser[] lasers;

    void Start()
    {
        lasers = GetComponentsInChildren<Laser>();
    }

    public void toggleLasers(bool on)
    {
        foreach(Laser l in lasers)
        {
            if (on) l.turnOn();
            else l.turnOff();
        }
    }
}
