﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PressurePlate : MonoBehaviour {

    public float massRequired = 1;

    public Laser[] laserTraps;
    public AudioClip onSound;
    public AudioClip offSound;

    private AudioSource audion;
    private Animator animator;
    private Renderer rend;

    [SerializeField] UnityEvent onAction = new UnityEvent();
    [SerializeField] UnityEvent offAction = new UnityEvent();

    [SerializeField] private bool isPressed = false;
    public float currentmass = 0;

    private void OnCollisionEnter(Collision collision) {
        currentmass += collision.transform.GetComponent<Rigidbody>().mass;
        
        if (currentmass > massRequired) {            
            if (!isPressed) {
                pressToggle(true);
            }
        }
    }

    private void OnCollisionExit(Collision collision) {
        
        currentmass -= collision.transform.GetComponent<Rigidbody>().mass;
        if (currentmass < massRequired) {            
            if (isPressed) {
                pressToggle(false);
            }
        }
    }

    private void pressToggle(bool on) {
        if (!on) {
            isPressed = false;
            foreach(Laser laser in laserTraps) {
                laser.turnOn();
            }
            offAction.Invoke();
            animator.SetBool("isActive", false);
            audion.clip = offSound;
            audion.Play();
            rend.materials[2].SetColor("_Color", Color.red);
        }
        else {
            isPressed = true;
            foreach (Laser laser in laserTraps) {
                laser.turnOff();
            }
            onAction.Invoke();
            animator.SetBool("isActive", true);
            audion.clip = onSound;
            audion.Play();
            rend.materials[2].SetColor("_Color", Color.cyan);
        }
    }

    // Use this for initialization
    void Start () {
        audion = GetComponent<AudioSource>();
        // audion.clip = clickSound;
        animator = GetComponent<Animator>();
        rend = GetComponent<Renderer>();
    }
	
	// Update is called once per frame
	void Update () {
        if (currentmass > massRequired) {
            if (!isPressed) {
                pressToggle(true);
            }
        }
        if (currentmass < massRequired) {
            if (isPressed) {
                pressToggle(false);
            }
        }
    }

    private void FixedUpdate() {
        
    }
}
