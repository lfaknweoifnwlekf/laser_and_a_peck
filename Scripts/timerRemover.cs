﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class timerRemover : MonoBehaviour {

	[SerializeField] private float timeTillDelete = 5;
	[SerializeField]private bool deleteEntirely = false;
	private ParticleSystem parts;
	private AudioSource audion;
	[SerializeField] private bool audioloop = false;



    private void Start()
    {
		parts = GetComponent<ParticleSystem>();		
		audion = GetComponent<AudioSource>();
		if (audion != null) audioloop = audion.loop;
    }

    void OnEnable () {
		StartCoroutine(countdown());
		if (parts != null) parts.enableEmission = true;
		if (audion != null) audion.loop = audioloop;
	}

    IEnumerator countdown() {
        yield return new WaitForSeconds(timeTillDelete);
        if (deleteEntirely)
        {
			Destroy(gameObject);
        }
        else
        {
			if (parts != null) parts.enableEmission = false;
			if(audion!=null) audion.loop = false;
		}
		
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
