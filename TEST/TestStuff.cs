﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestStuff : MonoBehaviour
{

    public bool loadNextLevel = false;
    public bool loadMenu = false;
    public bool loadLevel1 = true;
    public bool resetFBT = false;
    public bool applyFBT = false;

    [SerializeField] private Player player;
    private fullBodyAssign fbtMgr;

    // Start is called before the first frame update
    void Start()
    {
        player = Object.FindObjectOfType<Player>();
        fbtMgr = Object.FindObjectOfType<fullBodyAssign>();
        DontDestroyOnLoad(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        if (loadNextLevel)
        {
            loadNextLevel = false;
            player.loadNextLevel();
        }
        if (loadMenu)
        {
            loadMenu = false;
            player.loadLevel(1);
        }
        if (loadLevel1)
        {
            loadMenu = false;
            player.loadLevel(3);
        }
        if (resetFBT)
        {
            resetFBT = false;
            fbtMgr.resetFBTOffsets();
        }
        if (applyFBT)
        {
            applyFBT = false;
            fbtMgr.applyFBTOffsets();
        }
    }
}
